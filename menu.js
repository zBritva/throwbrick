public var welcomeLabel : GUIStyle;
public var customSkin : GUISkin;

private var optionsMode = false;

public var _bulletImpulse : float = 300;
public var _shootDelay : float = 1;

function OnGUI(){
    if(!optionsMode){
        //GUI.Label(new Rect(Screen.width / 2, 0, 50, 20),"Welcome",welcomeLabel);

        GUI.skin = customSkin;

        if(GUI.Button(new Rect(3, 3, Screen.width-6, 120),"Play Game")){
            Application.LoadLevel("test_scene");  //1
        }
           
        if(GUI.Button(new Rect(3, 126, Screen.width-6, 120),"Options")){
            optionsMode = true;
        }
                   
        if(GUI.Button(new Rect(3, 249, Screen.width-6, 120),"Quit")){
            Application.Quit();                   //2
        }
                   
    }else{
                                           
        //GUI.Label(new Rect(Screen.width / 2, 0, 50, 20), "Options",welcomeLabel);
                   
        GUI.skin = customSkin;
                   
        if(GUI.Button(new Rect(3, 3, Screen.width-6, 120),"<< Back")){
            optionsMode = false;
        }
           
    }  
                           
}