#pragma strict
//Время жизни объекта
public var liveTime = 60.0f;
private var startTime : float; 
public var fly: boolean;
private var needAngle : Quaternion;

function Start () {
	startTime = Time.time;
	fly = true;
	needAngle = Quaternion.Euler (Random.Range(-3,3), Random.Range(-3,3), Random.Range(-3,3));  
}

function Update () {
	//По истечению определенного в liveTime времени объект сам себя уничтожает 
	if ( Time.time > (startTime + liveTime) )
	{
		Destroy(gameObject);
	}
	if (fly)
	{
		transform.rotation *= needAngle;
	}
}

function OnCollisionEnter (collision : Collision) {
	//Если кирпич толкнулся не с окном, то завершаем вращение
	if (!collision.gameObject.CompareTag ("window"))
	{
		fly = false;
	}
}