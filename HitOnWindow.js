#pragma strict
public var welcomeLabel = "Score: ";
public var scoreCount = 0;

private var mainCamera : GameObject;
private var gui: GUIFunctions;

function Start () {
	mainCamera = GameObject.Find("Main Camera");
	gui = mainCamera.GetComponent(GUIFunctions);
}

function Update () {
	
}

function OnCollisionEnter (collision : Collision) {
	if (collision.gameObject.CompareTag ("brick") && !gui.win && !gui.loss)
	{
		var fracture : SimpleFracture;
		fracture = GetComponent(SimpleFracture);
		fracture.FractureAtPoint(collision.gameObject.transform.position,Vector3.up*100);

		scoreCount++;
		gui.win = true;
	}
	//Physics.IgnoreCollision(collider, collision.gameObject.collider);
}
