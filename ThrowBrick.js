private var brickImpulse : int; 
public var shootSpeed = 1; 
public var brick : GameObject; 
public var shotCount = 1;
//Максимальное значение угла поворота по осям
public var max_angle_x = 45.0;
public var max_angle_y = 30.0;
//public var min_angle_x = 0.0;
public var min_angle_y = 10.0;
//Чувствительность импульса
public var impulseSensitivity = 2;
//Минимальное и максимально возможное значение импульса
public var max_impulse = 25.0;
public var min_impulse = 3.0;
//Время последнего выстрала
private var lastShotTime : float; 
//Координаты при нажатии
private var mouse_down: Vector3;
//Координаты после отжатии
private var mouse_up: Vector3;
//Координаты при касании
private var touch_down :Vector2;
//координаты при пропадании касания
private var touch_up :Vector2;
//Время начала касания или клика мышки
private var start_time : float;
//Время окончания касания или клика мыши
private var finish_time : float;
//Последняя позиция мышки(касания) на экране
private var last_mouse_position : Vector2;

private var brick_clone_script : DestroyableBrick;

private var mainCamera : GameObject;

private var gui: GUIFunctions;

private var move_area : int;

private var lastTouchTime : float;

public var touchHold : float;

public var max_offsetForBrick : float;

private var current_offsetForBrick : float;

//Последнее касание
private var touch_last :Vector2;

function Start() 
{ 
	lastShotTime = 0.0; 
	lastTouchTime = 0.0;
	brickImpulse = min_impulse;
	current_offsetForBrick = 0;

	mainCamera = GameObject.Find("Main Camera");
	gui = mainCamera.GetComponent(GUIFunctions);
	
	//Определяем зону мышки(касания) для перемещения кирпича
	//В данном случае, нижняя четверть экрана
	move_area = Screen.height / 4;
	Debug.Log("MOVE AREA:" + move_area);
	Debug.Log("SH:" + Screen.height);
	
	mainCamera.transform.position.x = transform.position.x;
} 

function Update () 
{ 
	if(Input.GetMouseButtonDown(0)) 
		Debug.Log("MP:" + Input.mousePosition.x + " "+ Input.mousePosition.y);
	//Перемещение кирпича
	//Оперделение положения начального касания и времени
	if (Input.touchCount > 0 &&  Input.GetTouch(0).phase == TouchPhase.Began)
	{
		//Сохраняем время последнего касания
		lastTouchTime = Time.time; 
		//Запоминаем координаты касания на текущем кадре
		touch_last = Input.GetTouch(0).position;
	}
	
	//Пока касание перемещается отслеживаем изменение
	if (Input.touchCount > 0 &&  Input.GetTouch(0).phase == TouchPhase.Moved)
	{
		//Если прошло touchHold -количество времени, то получаем вектор для перемещения
		if (Time.time>(lastTouchTime + touchHold)) 
		{ 
			var move :  Vector2;
			move.x = touch_last.x - Input.GetTouch(0).position.x;
			//move.y = touch_last - Input.GetTouch(0).position.y;
			
		
			//Если текущее каксание находиться в зоне пемещения кирпича и не вышел за пределы,
			//то двигаем кирпич и камеру по оси X 
			if (Input.GetTouch(0).position.y < move_area && Mathf.Abs(current_offsetForBrick + move.x/30) < max_offsetForBrick )
			{
				current_offsetForBrick += move.x / 30;
				//перемещаем кирпич
				transform.Translate(Vector3.left * Time.deltaTime/2 * (move.x / 30));
				transform.rotation = Quaternion.AngleAxis(0, Vector3.zero);
				//Перемещаем камеру
	    		//mainCamera.transform.position.x = transform.position.x;
				//mainCamera.transform.rotation = Quaternion.AngleAxis(0, Vector3.zero);
			}
		}
		//Сохраняем время последнего касания
		lastTouchTime = Time.time; 
		//Запоминаем координаты касания на текущем кадре
		touch_last = Input.GetTouch(0).position;
	}
		
	//Отслеживаем местонахождение мышки и состояние кнопки
	//if ((Input.mousePosition.y < move_area) && Input.GetMouseButtonDown(0))
    //{
    //	Debug.Log("MOVE ON");
    //	if (Input.mousePosition.x > (Screen.width/2 + Screen.width/10) )
    //	{
    //		//Перемещаем кирпич
    //		transform.Translate(Vector3.left * Time.deltaTime * (-2) );
    //		transform.rotation = Quaternion.AngleAxis(0, Vector3.zero);
    //		//Перемещаем камеру
    //		//mainCamera.transform.position.x = transform.position.x;
	//		//mainCamera.transform.rotation = Quaternion.AngleAxis(0, Vector3.zero);
    //	}
    //	if (Input.mousePosition.x < (Screen.width/2 - Screen.width/10) )
    //	{
    //		//Перемещаем кирпич
    //		transform.Translate(Vector3.left * Time.deltaTime * 2);
    //		transform.rotation = Quaternion.AngleAxis(0, Vector3.zero);
    //		///Перемещаем камеру
    //		//mainCamera.transform.position.x = transform.position.x;
	//		//mainCamera.transform.rotation = Quaternion.AngleAxis(0, Vector3.zero);
    //	}
    //	last_mouse_position = Input.mousePosition;
    //}
	//Конец кода перемещения кирпича
	
	//Бросок кирпича
	//Сохранение координат касания
	if (Input.touchCount > 0 &&  Input.GetTouch(0).phase == TouchPhase.Began)
	{
		if (shotCount == 0 && !gui.loss && !gui.win)
		{
			gui.loss = true;
			return;
		}
		start_time = Time.time;
		touch_down = Input.GetTouch(0).position;
		mouse_down.x = touch_down.x;
		mouse_down.y = touch_down.y;
	}
	//Сохранение координат пропадания касания
	if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
	{
		//Запуск кирпича))
		if (Time.time>(lastShotTime + shootSpeed)) 
		{ 
			finish_time = Time.time;
			touch_up = Input.GetTouch(0).position;
			//Если пользователь отпустил касание в пределах зоны перемещения кирпича, то выстрел не происходит
			//И минимальная длинна вектора инпульса должна быть больше 30 и положительным(что бы кирпич не летел назад), 
			//для предотвращения броска кирпича, простым нажатием по экрану
			if (touch_up.y > move_area && ( touch_up.y - touch_down.y )> 30 )
			{
				mouse_up.x = touch_up.x;
				mouse_up.y = touch_up.y;
				if (shotCount == 0  && !gui.loss && !gui.win)
				{
		    		gui.loss = true;
					return;
				}
				
				if (shotCount > 0 && !gui.loss  && !gui.win)
				{
					shotBrick();
					shotCount--;
				}
			}
		} 
	}
	//Конец кода броска кирпича
}


function OnMouseDown()
{
	if (shotCount == 0 && !gui.loss && !gui.win)
	{
		gui.loss = true;
		return;
	}
	//Debug.Log("On mouse down");
	//Сохраняем координаты точки касания
	start_time = Time.time;
	mouse_down = Input.mousePosition;
	//Debug.Log("Mouse position: "+mouse_down.x+" "+mouse_down.y);
}

function OnMouseOver () {

}

function OnMouseUp()
{
	if (Time.time>(lastShotTime + shootSpeed)) 
	{ 
		finish_time = Time.time;
		//Точка где отпускается мышка
		mouse_up = Input.mousePosition;
		if (mouse_up.y > move_area)
		{
			if (shotCount == 0  && !gui.loss && !gui.win)
			{
	    		gui.loss = true;
				return;
			}
			
			if (shotCount > 0 && !gui.loss  && !gui.win)
			{
				shotBrick();
				shotCount--;
			}
		}
	} 
}

function shotBrick()
{
	//Направление кирпича
	var brick_dirrection :  Quaternion;

	//Получение вектора мышки
	var mouse_x : int;
	var mouse_y : int; 
	//Вычитаем из высоты экрана, так как ось y у экрана инвертирован
	mouse_y = mouse_down.y - mouse_up.y;
	mouse_x = mouse_up.x - mouse_down.x;
	
	var impulse : int;
	impulse = Mathf.Sqrt(mouse_x*mouse_x + mouse_y*mouse_y);
	
	impulse = impulse / impulseSensitivity;
	if (impulse < min_impulse)
	{
		impulse = min_impulse;
	}
	if (impulse > max_impulse)
	{
		impulse = max_impulse;
	}
	
	//Масштабирование значения вектора мышки, для задания угла поворота мышки
	var x_mod : float;
	var y_mod : float;
	//Масштабирование значения длинны вектора мышки
	var length_mod : float;
	
	x_mod = max_angle_x*2 / Screen.currentResolution.width ;
	y_mod = max_angle_y*2 / Screen.currentResolution.height ;
	length_mod = 1;

	//Клонируем направление у оригинала
	brick_dirrection = transform.rotation;
	//тут задается направление полета кирпича, необходимо ограничить угол полета по высоте и направлению +- 20
	
	if (Mathf.Abs(mouse_x*x_mod) > max_angle_x)
	{
		brick_dirrection *= Quaternion.AngleAxis(max_angle_x*Mathf.Sign(mouse_x), Vector3.up);
	}
	else
	{
		brick_dirrection *= Quaternion.AngleAxis(mouse_x*x_mod, Vector3.up);
	}
	
	if (Mathf.Abs(mouse_y*y_mod) > max_angle_y)
	{
		brick_dirrection *= Quaternion.AngleAxis(max_angle_y*Mathf.Sign(mouse_y), Vector3.right);
	} else
	if (Mathf.Abs(mouse_y*y_mod) < min_angle_y)
	{
		brick_dirrection *= Quaternion.AngleAxis(min_angle_y*Mathf.Sign(mouse_y), Vector3.right);
	}
	else
	{
		brick_dirrection *= Quaternion.AngleAxis(mouse_y*y_mod, Vector3.right);
	}
	
	var brick_clone : GameObject; 
	var brick_direct : Quaternion;
	brick_direct = transform.rotation;
	brick_clone = Instantiate(brick, (transform.position+Vector3.up*0.3), brick_dirrection);
	brick_clone.tag = "brick";
	Physics.IgnoreCollision(brick_clone.collider, collider); 
	brick_clone.rigidbody.AddForce(brick_clone.transform.forward*impulse, ForceMode.Impulse); 
	lastShotTime = Time.time; 
	brick_clone_script = brick_clone.GetComponent(DestroyableBrick);
	brick_clone_script.fly = true;
}

function OnGUI()
{
	GUI.Label(new Rect( 3, Screen.height - 18, 150, 30),"Brick count: "+shotCount);
}