#pragma strict

var win : boolean;
var loss : boolean;


function Start () {
	win  = false;
	loss = false;
}

function Update () {

}

function OnGUI()
{
	if (GUI.Button(new Rect( (Screen.width) / 2 , 3, (Screen.width) / 2-3 , 80),"Exit"))
	{
    	Application.LoadLevel(0);
    }
    if (win && !loss)
	{
		GUI.Label(new Rect( (Screen.width) / 2 - 30 , (Screen.height)/2, 150, 30),"You won!");
		if (GUI.Button(new Rect( (Screen.width) / 2 -150, (Screen.height)/2 + 35,150,30),"Exit"))
		{
            Application.LoadLevel(0);
        }
        if (GUI.Button(new Rect( (Screen.width) / 2 +3 , (Screen.height)/2 + 35,150,30),"Next"))
		{
            Application.LoadLevel(1);
        }
	}
	if (loss && !win)
	{
		GUI.Label(new Rect( (Screen.width) / 2 -30 -3, (Screen.height)/2, 150, 30),"You loss!");
		if (GUI.Button(new Rect( (Screen.width) / 2 -150, (Screen.height)/2 + 35,150,30),"Exit"))
		{
            Application.LoadLevel(0);
        }
        if (GUI.Button(new Rect( (Screen.width) / 2 +3 , (Screen.height)/2 + 35,150,30),"Replay"))
		{
            Application.LoadLevel(1);
        }
	}
}


